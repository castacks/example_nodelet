#ifndef _EXAMPLE_NODELET_H_
#define _EXAMPLE_NODELET_H_

#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <base/BaseNodelet.h>

class ImageNodelet : public BaseNodelet {
 private:
  bool first_image;
  sensor_msgs::Image current_image;
  ros::Subscriber image_sub;
  ros::Publisher blue_image_pub;

 public:
  ImageNodelet();
  void image_callback(sensor_msgs::ImageConstPtr image);

  virtual bool initialize();
  virtual bool execute();
  virtual ~ImageNodelet();
};

#endif
