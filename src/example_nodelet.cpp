/* An example nodelet using the BaseNodelet class.
 *
 * author: John Keller jkeller2@andrew.cmu.edu
 *
 */
#include <pluginlib/class_list_macros.h>
#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <base/BaseNodelet.h>
#include "example_nodelet/example_nodelet.h"


ImageNodelet::ImageNodelet()
  : BaseNodelet("ImageNodelet")
  , first_image(true){

}

/*!
 * \brief Callback that makes an image more blue.
 */
void ImageNodelet::image_callback(sensor_msgs::ImageConstPtr image){
  // initialize current_image
  if(first_image){
    current_image.height = image->height;
    current_image.width = image->width;
    current_image.encoding = image->encoding;
    current_image.is_bigendian = image->is_bigendian;
    current_image.step = image->step;
    current_image.data.resize(image->data.size());
    first_image = false;
  }

  // make each pixel a little more blue
  for(int i = 0; i < current_image.data.size(); i++){
    uint8_t x = image->data[i];
    if(i%3 == 2)
      x = std::min(x + 100, 255);
    current_image.data[i] = x;
  }

  blue_image_pub.publish(current_image);
}


/*!
 * \brief Initialize ROS related objects here. Get a pointer to the node handle
 * and create a publisher and subscriber.
 */
bool ImageNodelet::initialize(){
  ros::NodeHandle* nh = get_node_handle();

  image_sub = nh->subscribe("/camera/image_raw", 10, &ImageNodelet::image_callback, this);
  blue_image_pub = nh->advertise<sensor_msgs::Image>("/nodelet/camera/image_blue", 10);
  return true;
}

/*!
 * \breif Execute code here.
 */
bool ImageNodelet::execute(){
  ROS_INFO("EXECUTE CALLED");
  return true;
}

/*!
 * \brief Do any cleanup here.
 */
ImageNodelet::~ImageNodelet(){

}

PLUGINLIB_EXPORT_CLASS(ImageNodelet, nodelet::Nodelet)
