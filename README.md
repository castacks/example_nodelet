This package is an example of a BaseNodelet. Comments on how to derive from BaseNode and use each function can be found in src/example_nodelet.cpp. The CMakeLists.txt, package.xml, and example_nodelet.xml also show what is required to build the code.


Author: John Keller jkeller2@andrew.cmu.edu slack: kellerj
